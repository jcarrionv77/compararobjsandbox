var fs = require("fs");
var nrc = require('node-run-cmd');
var path = require('path')
var unique = require('array-unique');
var sleep = require('sleep');

const Json2csvParser = require('json2csv').Parser;

var HashMap = require('hashmap');

var html = '<html><table border=&quot;1&quot;>';

var htmlTanspuesto = '<html><table border=&quot;1&quot;>';


const fieldsCSV = ['Name', 'JCV_fld_sandbox__c', 'JCV_fld_field__c','JCV_fld_object__c'];



const args = process.argv;

console.log(args[2]);

const miObjeto = args[2];


var directorioObj = 'tmp/'+ miObjeto; 
var nombreHtml =  directorioObj + '/'+ miObjeto + '.html'; 

var createObj = 'mkdir ' + directorioObj;

var rmObj = 'rm -r ' + directorioObj;

nrc.run('mkdir tmp');

nrc.run(rmObj);
sleep.sleep(1);
nrc.run(createObj);

function readFiles(){


	console.log('readFiles en  ' + directorioObj);


	var files = fs.readdirSync(directorioObj);
	var fieldsArray = new Array();

	var orgsArray = new Array();
	
	var camposArray = new Array(); 

	files.forEach(file => {

		try{



			//console.log('file ' + file);
			if(path.extname(file) == '.json' && file != 'DevHub.json'  &&  file != 'repaudit.json')
			{
				//console.log('file ' + file);

				var org = { name: '', fields: []};
				org.name = file;
				
				var nameOrg = file.substring(0,file.length-5);

				var MyFile = fs.readFileSync(directorioObj+'/'+file);

				if(MyFile.length>0)
				{

					var jsonContent = JSON.parse(MyFile);

					var fields = new Array();

					for (var i =0; i<jsonContent.result.fields.length; i++)
					{
						var field = {};
						
						var campo = new Object()

						field.name = jsonContent.result.fields[i].name;

						
						campo.Name= nameOrg + jsonContent.result.fields[i].name;		
						campo.JCV_fld_sandbox__c= nameOrg;
						campo.JCV_fld_field__c= jsonContent.result.fields[i].name;
						campo.JCV_fld_object__c = miObjeto;
						
						
						camposArray.push(campo);

						fields.push(field);
						org.fields.push(jsonContent.result.fields[i].name);
						fieldsArray.push(jsonContent.result.fields[i].name);
					}

					orgsArray.push(org);
				}
			}
		}
		catch (err) {
			console.error('Error en files.forEach');
  			console.error(err);
		}
	});


	var camposJSON = JSON.parse(JSON.stringify(camposArray));

	//console.error(camposJSON);

	try {
		const json2csvParser = new Json2csvParser({ fieldsCSV });
		const csv = json2csvParser.parse(camposJSON);
		 
		fs.writeFileSync('bulk.csv', csv);

		var cargaBulk = 'sfdx force:data:bulk:upsert -s JCV_obj_fieldAudit__c -f ./bulk.csv -i Name -u repaudit';

		//console.error('traza1');
		nrc.run(cargaBulk);
		//console.error('traza2');

	}
	catch (err) {
		console.error('Error en CSV bulk');
  		console.error(err);
	}

	unique(fieldsArray);
	var sortFieldsArray = fieldsArray.sort();

	var fieldResult = new Array();
	
	var map = new HashMap();

	for(var i=0; i< sortFieldsArray.length; i++){
		map.set(sortFieldsArray[i], i);
	}

	for (var k=0; k<orgsArray.length;k++){
	
		var camposOrg = new Array(sortFieldsArray.length);  

		for(j=0;j<orgsArray[k].fields.length;j++)
		{
			camposOrg[map.get(orgsArray[k].fields[j])] = 'si';
		}
		fieldResult.push(camposOrg);
	}
	
	/*
	html = html + '<tr><th>Org</th>';
	for(var i=0; i< sortFieldsArray.length; i++){
		html = html + '<th>' + sortFieldsArray[i] + '</th>';
	}
	html = html + '</tr>';
	for (var k=0; k<orgsArray.length;k++){
		html = html + '<tr><td>' + orgsArray[k].name + '</td>';
		for (var p=0; p< sortFieldsArray.length; p++){
			html = html + '<td>' + fieldResult[k][p] + '</td>';
		}
		html = html + '</tr>';
	}
	html = html + '</table></html>';

	console.log('nombreHtml ' + nombreHtml);*/


	//fs.writeFileSync(nombreHtml, html);

	htmlTanspuesto = '<html><table border=&quot;1&quot;>';
	htmlTanspuesto = htmlTanspuesto + '<tr><th>Fields</th>';

	for (var k=0; k<orgsArray.length;k++){

		var orgName = orgsArray[k].name;
		htmlTanspuesto = htmlTanspuesto + '<th>' + orgName.substring(0,orgName.length-5) + '</th>';
	}
	html = html + '</tr>';
	for(var i=0; i< sortFieldsArray.length; i++){

		if(sortFieldsArray[i].includes("__c"))
		{	

			htmlTanspuesto = htmlTanspuesto + '<tr><td>' + sortFieldsArray[i] + '</td>';
			for (var k=0; k<orgsArray.length;k++){

				if(fieldResult[k][i] == 'undefined' || fieldResult[k][i] == '' || fieldResult[k][i] == null)
					htmlTanspuesto = htmlTanspuesto + '<td>' + '' + '</td>';
				else
					htmlTanspuesto = htmlTanspuesto + '<td>' + fieldResult[k][i] + '</td>';
			}
			htmlTanspuesto = htmlTanspuesto + '</tr>';
		}
		
	}
	htmlTanspuesto = htmlTanspuesto + '</table></html>';
	fs.writeFileSync(nombreHtml, htmlTanspuesto);


}

function describeAccount(value) {
    return new Promise((resolve) => {
    	var cmd = './describeAccount.sh ' + value + ' ' + miObjeto;
		nrc.run(cmd)
			.then(function(exitCodes) {
				sleep.sleep(2);
				//console.log('fin ' + cmd);
				resolve("Fin describeAccount" );
				 
			}, function(err) {
				  console.log('Command failed to run with error: ', err);
			});

      });
    }


function describeAccountSync(value) {
		var cmd = './describeAccount.sh ' + value + ' ' + miObjeto;
		nrc.run(cmd);
    }


nrc.run('./listOrg.sh')
	.then(function(exitCodes) {
  		console.log('Recuperado listado de Orgs');
  		var promises = [];
		var contents = fs.readFileSync("tmp/MyOrgList.json");
		var jsonContent = JSON.parse(contents);
		
		
		for (var i =0; i<jsonContent.result.nonScratchOrgs.length; i++){
			promises.push(describeAccount(jsonContent.result.nonScratchOrgs[i].alias));
		}

		Promise.all(promises)
		    .then(() => {
		    	console.log('Ficheros descargados');
				sleep.sleep(20);
				console.log('20 segundos despues  ' );
		    	readFiles();
		    	console.log('HTML generado');
		    })
		    .catch((e) => {
		        // handle errors here
		    });

		



	}, function(err) {
  		console.log('Command failed to run with error: ', err);
});


